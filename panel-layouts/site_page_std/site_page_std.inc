<?php

/**
 * @file
 * Implementation for the layout of the site's standard page
 *
 * Regions:
 * - $plugin['regions']['toolbar'] = Toolbar
 * - $plugin['regions']['header']: Items for the header region.
 * - $plugin['regions']['navigation'] = Navigation
 * - $plugin['regions']['content']: The main content of the current page.
 * - $plugin['regions']['sidebar']: Items for the first sidebar.
 * - $plugin['regions']['footer_firstcolumn'] = Footer first column
 * - $plugin['regions']['footer_secondcolumn'] = Footer second column
 * - $plugin['regions']['footer_thirdcolumn'] = Footer third column
 * - $plugin['regions']['footer_fourthcolumn'] = Footer fourth column
 * - $plugin['regions']['footer']: Items for the footer region.
 *
 */

// Plugin definition
$plugin = array(
  'title' => t('Standard Site Page'),
  'category' => t('Pepper Layouts'),
  'icon' => 'site_page_std.png',
  'theme' => 'site_page_std',
  'regions' => array(
    'toolbar' => t('Toolbar'),
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'header' => t('Header'),
    'content' => t('Content'),
    'sidebar' => t('Sidebar'),
    'footer_firstcolumn' => t('Footer first column'),
    'footer_secondcolumn' => t('Footer second column'),
    'footer_thirdcolumn' => t('Footer third column'),
    'footer_fourthcolumn' => t('Footer fourth column'),
    'footer' => t('Footer'),
  ),
);