<?php
/**
 * @file
 * Template for a panel layout of the site's standard page.
 *
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['toolbar'] = Toolbar
 *   - $content['header']: Items for the header region.
 *   - $content['navigation'] = Navigation
 *   - $content['content']: The main content of the current page.
 *   - $content['sidebar']: Items for the first sidebar.
 *   - $content['footer_firstcolumn'] = Footer first column
 *   - $content['footer_secondcolumn'] = Footer second column
 *   - $content['footer_thirdcolumn'] = Footer third column
 *   - $content['footer_fourthcolumn'] = Footer fourth column
 *   - $content['footer']: Items for the footer region.
 */
?>

  <div class="panel-display panel-site-page-std <?php if (!empty($content['sidebar'])) { print 'sidebar';} ?>" id="page">

    <section id="toolbar">
      <?php print $content['toolbar']; ?>
    </section> <!-- /#toolbar-->

    <header id="header" role="banner">
      <?php print $content['header']; ?>
    </header> <!-- /#header -->

    <nav id="navigation">
      <?php print $content['navigation']; ?>
    </nav> <!-- /#navigation-->

    <div id="main">

      <div id="content" role="main">
        <a id="main-content"></a>
        <?php print $content['content']; ?>
      </div> <!-- /#content -->

      <aside id="sidebar" role="complementary">
        <?php print $content['sidebar']; ?>
      </aside> <!-- /#sidebar -->

    </div> <!-- /#main -->
    
    <div id="footer-wrapper" class="">

      <div id="footer-links">
        <div id="footer-links-first"><?php print $content['footer_firstcolumn']; ?></div>
        <div id="footer-links-second"><?php print $content['footer_secondcolumn']; ?></div>
        <div id="footer-links-third"><?php print $content['footer_thirdcolumn']; ?></div>
        <div id="footer-links-fourth"><?php print $content['footer_fourthcolumn']; ?></div>
      </div> <!-- /#footer-links -->

      <footer id="footer" role="contentinfo">
        <?php print $content['footer']; ?>
      </footer> <!-- /#footer -->

    </div> <!-- /#footer-wrapper -->

  </div> <!-- /#page -->
