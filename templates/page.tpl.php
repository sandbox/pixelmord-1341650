<?php

/**
 * @file
 * Pepper theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['toolbar'] = Toolbar
 * - $page['header']: Items for the header region.
 * - $page['navigation'] = Navigation
 * - $page['content']: The main content of the current page.
 * - $page['sidebar']: Items for the first sidebar.
 * - $page['footer_firstcolumn'] = Footer first column
 * - $page['footer_secondcolumn'] = Footer second column
 * - $page['footer_thirdcolumn'] = Footer third column
 * - $page['footer_fourthcolumn'] = Footer fourth column
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>

  <div id="page">

    <?php if ($page['toolbar']): ?>
      <section id="toolbar">
        <?php print render($page['toolbar']); ?>
      </section> <!-- /#toolbar-->
    <?php endif; ?>

    <header id="header" role="banner">

      <?php print render($page['header']); ?>

      <div id="identity">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

        <?php if ($site_name || $site_slogan): ?>
          <hgroup id="name-and-slogan">
            <?php if ($site_name): ?>
              <?php if ($title): ?>
                <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>><strong>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                </strong></div>
              <?php else: /* Use h1 when the content title is empty */ ?>
                <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                </h1>
              <?php endif; ?>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <h2 id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>><?php print $site_slogan; ?></h2>
            <?php endif; ?>
          </hgroup> <!-- /#name-and-slogan -->
        <?php endif; ?>
      </div>
      
    </header> <!-- /#header -->

    <?php if ($page['navigation']): ?>
      <nav id="navigation">
        <?php print render($page['navigation']); ?>
      </nav> <!-- /#navigation-->
    <?php endif; ?>

    <div id="main">

      <div id="content" role="main">
        <?php print $messages; ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php if (!empty($tabs['#primary']) || !empty($action_links)): ?>
          <nav>
            <?php if (!empty($tabs['#primary'])): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
            <?php print render($page['help']); ?>
            <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
          </nav>
        <?php endif; ?>
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>
      </div> <!-- /#content -->

      <?php if ($page['sidebar']): ?>
        <aside id="sidebar" role="complementary">
          <?php print render($page['sidebar']); ?>
        </aside> <!-- /#sidebar -->
      <?php endif; ?>

    </div> <!-- /#main -->
    
    <div id="footer-wrapper" class="">

      <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn']): ?>
        <div id="footer-links">
          <div id="footer-links-first"><?php print render($page['footer_firstcolumn']); ?></div>
          <div id="footer-links-second"><?php print render($page['footer_secondcolumn']); ?></div>
          <div id="footer-links-third"><?php print render($page['footer_thirdcolumn']); ?></div>
          <div id="footer-links-fourth"><?php print render($page['footer_fourthcolumn']); ?></div>
        </div> <!-- /#footer-links -->
      <?php endif; ?>

      <?php if ($page['footer']): ?>
        <footer id="footer" role="contentinfo">
          <?php print render($page['footer']); ?>
        </footer> <!-- /#footer -->
      <?php endif; ?>

    </div> <!-- /#footer-wrapper -->

  </div> <!-- /#page -->
