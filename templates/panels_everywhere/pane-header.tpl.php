<?php
/**
 * @file
 *
 * Theme implementation to display the header block on a Drupal page.
 *
 * This utilizes the following variables thata re normally found in
 * page.tpl.php:
 * - $logo
 * - $front_page
 * - $site_name
 * - $front_page
 * - $site_slogan
 * - $search_box
 *
 * Additional items can be added via theme_preprocess_pane_header(). See
 * template_preprocess_pane_header() for examples.
 */
 ?>
<div id="identity">
	<?php if ($logo): ?>
	  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
	    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
	  </a>
	<?php endif; ?>

	<?php if ($site_name || $site_slogan): ?>
	  <hgroup id="name-and-slogan">
	    <?php if ($site_name): ?>
	      <?php if ($title): ?>
	        <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>><strong>
	          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
	        </strong></div>
	      <?php else: /* Use h1 when the content title is empty */ ?>
	        <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
	          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
	        </h1>
	      <?php endif; ?>
	    <?php endif; ?>

	    <?php if ($site_slogan): ?>
	      <h2 id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>><?php print $site_slogan; ?></h2>
	    <?php endif; ?>
	  </hgroup> <!-- /#name-and-slogan -->
	<?php endif; ?>
</div>